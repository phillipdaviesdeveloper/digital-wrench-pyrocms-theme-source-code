<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget_PersonalNote extends Widgets
{
    public $title = 'Personal Note';
    public $description = 'Display the personal site note';
    public $author = 'Phillip Davies';
    public $website = 'http://digitalwrench.co.uk';
    public $version = '1.0.0';

    public $fields = array(
            array(
                'field' => 'note',
                'label' => 'Note Content',
                'rules' => 'required'
            )
        );


    /**
     * the $options param is passed by the core Widget class.  If you have
     * stored options in the database,  you must pass the paramater to access
     * them.
     */
    public function run($options)
    {
        if(empty($options['note']))
        {
            //return an array of data that will be parsed by views/display.php
            return array('output' => '');
        }

        // Store the feed items
        return array('output' => $options['note']);
    }


}