require.config({
    baseUrl: "/addons/default/themes/wrench/js/",
    shim: {
        jquery: {
            exports: 'jQuery'
        },
        foundation: {
            deps: ["jquery"]
        },
        "foundation.alerts": {
            deps: ["foundation"]
        },
        "foundation.clearing": {
            deps: ["foundation"]
        },
        "foundation.cookie": {
            deps: ["foundation"]
        },
        "foundation.dropdown": {
            deps: ["foundation"]
        },
        "foundation.forms": {
            deps: ["foundation"]
        },
        "foundation.joyride": {
            deps: ["foundation"]
        },
        "foundation.magellan": {
            deps: ["foundation"]
        },
        "foundation.orbit": {
            deps: ["foundation"]
        },
        "foundation.placeholder": {
            deps: ["foundation"]
        },
        "foundation.reveal": {
            deps: ["foundation"]
        },
        "foundation.section": {
            deps: ["foundation"]
        },
        "foundation.tooltips": {
            deps: ["foundation"]
        },
        "foundation.topbar": {
            deps: ["foundation"]
        }
    },

    paths: {
        jquery: [
            "//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min",
            "vendor/jquery"
        ],
        foundation: "foundation/foundation",
        "foundation.alerts": "foundation/foundation.alerts",
        "foundation.clearing": "foundation/foundation.clearing",
        "foundation.cookie": "foundation/foundation.cookie",
        "foundation.dropdown": "foundation/foundation.dropdown",
        "foundation.forms": "foundation/foundation.forms",
        "foundation.joyride": "foundation/foundation.joyride",
        "foundation.magellan": "foundation/foundation.magellan",
        "foundation.orbit": "foundation/foundation.orbit",
        "foundation.placeholder": "foundation/foundation.placeholder",
        "foundation.topbar": "foundation/foundation.topbar",
        "foundation.reveal": "foundation/foundation.reveal",
        "foundation.section": "foundation/foundation.section",
        "foundation.tooltips": "foundation/foundation.tooltips",
        "foundation.topbar": "foundation/foundation.topbar"
    }
});

requirejs(['jquery', 'modules/log', 'modules/util', 'modules/anim-position', 'foundation'
//    "foundation.alerts",
//    "foundation.clearing",
//    "foundation.cookie",
//    "foundation.dropdown",
//    "foundation.forms",
//    "foundation.joyride",
//    "foundation.magellan",
//    "foundation.orbit",
//    "foundation.placeholder",
//    "foundation.topbar",
//    "foundation.reveal",
//    "foundation.section",
//    "foundation.tooltips",
//    "foundation.topbar",
], function ($,log, util, animpos) {

    $(document).foundation();


    log.log('---------------------------------------- ******************* ------------------------------------------ \n' +
        'Hi! I see you are taking a look at my code, feel free to add me on twitter @phillipdavies or if you ' +
        'just want to look at the code you can download all the site files from git.digitalwrench.co.uk :) - Phill ' +
        '\n---------------------------------------- ******************* ------------------------------------------' +
        '\n' +
        '\n' +
        '\n' +
        '\n' +
        'This site was built in pyrocms, using zurb foundation, compass and require.js.')

    try
    {

//        if($('.logo--menu').length)
//        {
//            var logoAnim = new animpos;
//            logoAnim.init({
//                target: '.logo--menu',
//                anim: 'is--sliding',
//                pos: '.splash--homepage',
//                modifer: -80,
//                type: 'removeClass',
//                on:  'scroll'
//            });
//
//            var logoAnimTimeout = setInterval(function() {
//                if(logoAnim.watchTrigger(logoAnim))
//                {
//                    clearInterval(logoAnimTimeout);
//                }
//            }, 100);
//        }

        if($('.skills-bar--span').length)
        {
            var skillsAnim = new animpos;
                skillsAnim.init({
                    target: '.skills-bar--span',
                    anim: 'is--0-percent',
                    pos: '.skills-bar'
                });
            var skillsAnimTimeout = setInterval(function() {
                if(skillsAnim.watchTrigger(skillsAnim))
                {
                    clearInterval(skillsAnimTimeout);
                }
            }, 100);
        }


        if($('.footer--picture img').length)
        {
            var footerAnim = new animpos;
                footerAnim.init({
                    target: '.footer--picture img',
                    anim: 'is--scaled',
                    pos: '.footer',
                    modifer: 0,
                    type: 'addClass'
                });
            var footerAnimTimeout = setInterval(function() {
                if(footerAnim.watchTrigger(footerAnim))
                {
                    clearInterval(footerAnimTimeout);
                }
            }, 100);
        }

    }
    catch(e)
    {
        log.log(e);
    }


    $('.logo--splash').removeClass('is--blurred');


});
