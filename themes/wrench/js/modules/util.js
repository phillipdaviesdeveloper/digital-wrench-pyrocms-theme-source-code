define(['modules/log'], function(log)
    {

        /************************************
        //
        // This is a useful module of commonly used functions
        // which are global and require no other dependencies other than
        // outputting / framework / organisational tools.
        //
        // Essentially anything added here should contain no requirement to know
        // the specifics of the data passed and as such these methods should be quite simple.
        //
        ************************************/

        var that;

        function util()
        {


            // provides object scope reference in methods
            that = this;


            //ensure animationEngine the module is initialised correctly
            if (!(this instanceof util)) {
                throw new TypeError("Util constructor cannot be called as a function.");
            }

            // properties





            // "Private" methods

            /*************************
            //
            // These methods are used by the module specifically and have no external global use
            // or indeed any use by the client code.
            //
            *************************/

        }




        // methods

        /*************************
        //
        // These methods are essentially the public getter, setter type to
        // be used by the client coder
        //
        *************************/

        util.prototype = {


                /****************************
                //
                //  Applies non specific utilities for general usage
                //
                ****************************/

                init: function()
                {
                    // allows [12,3,34,6,33,23].max() to return the highest number in an array without looping manually
                    Array.prototype.max = function(){return Math.max.apply(Math,this)}

                    // allows [12,3,34,6,33,23].max() to return the lowest number in an array without looping manually
                    Array.prototype.min = function(){return Math.min.apply(Math,this)}
                },

                // methods


                /*************************
                //
                // Accepts an array and returns the amount of unique items specified from the array.
                //
                // For use when random items are required from a dataset and the same item from the array cannot be duplicated
                //
                // returns an array of the selected items.
                //
                **************************/
                randomUniqueArrayItems: function(list, amount)
                {

                        if (list === undefined) { throw new Error('Array required as the first argument') }

                        var length = list.length;
                        var loop = (amount >= length || amount === undefined ) ? length : amount;

                        this.indexes = [];
                        this.result = [];

                        while (length--) {
                            this.indexes[this.indexes.length] = length;
                        }

                        while(loop > 0)
                        {
                            var rand = Math.floor(Math.random() * this.indexes.length),
                                item = list[this.indexes[rand]];

                            this.indexes.splice(rand, 1);

                            loop--;

                            this.result.push(item);
                        }

                        return this.result;
                },





                /*************************
                //
                // Polyfill for multi browser request frame animation
                //
                // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
                // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
                //
                // requestAnimationFrame polyfill by Erik Möller
                // fixes from Paul Irish and Tino Zijdel
                //
                *************************/

                requestFrame: function() {
                    var lastTime = 0;
                    var vendors = ['ms', 'moz', 'webkit', 'o'];
                    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                            || window[vendors[x]+'CancelRequestAnimationFrame'];
                    }

                    if (!window.requestAnimationFrame)
                        window.requestAnimationFrame = function(callback, element) {
                            var currTime = new Date().getTime();
                            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                                timeToCall);
                            lastTime = currTime + timeToCall;
                            return id;
                        };

                    if (!window.cancelAnimationFrame)
                        window.cancelAnimationFrame = function(id) {
                            clearTimeout(id);
                        };
                },

            /********************
             *
             *  Allows us to use .bind which is useful in objects and making calls, e.g. calling a timeout in an object:
             *
             *
             *
             */
                bindPolyFill: function() {
                    if (!Function.prototype.bind) {
                        Function.prototype.bind = function (oThis) {
                            if (typeof this !== "function") {
                                // closest thing possible to the ECMAScript 5 internal IsCallable function
                                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
                            }

                            var aArgs = Array.prototype.slice.call(arguments, 1),
                                fToBind = this,
                                fNOP = function () {},
                                fBound = function () {
                                    return fToBind.apply(this instanceof fNOP && oThis
                                        ? this
                                        : oThis,
                                        aArgs.concat(Array.prototype.slice.call(arguments)));
                                };

                            fNOP.prototype = this.prototype;
                            fBound.prototype = new fNOP();

                            return fBound;
                        };
                    }
                },

                /**
                 * Returns a random number between min and max
                 */
                getRandomArbitary: function (min, max) {
                    return Math.random() * (max - min) + min;
                },

                /**
                 * Returns a random integer between min and max
                 * Using Math.round() will give you a non-uniform distribution!
                 */
                getRandomInt: function (min, max) {
                    return Math.floor(Math.random() * (max - min + 1)) + min;
                }


        }




        // RequireJS return


        /**************************
        //
        // Usually the methods would be returned here, but as we are using a
        // object and a protype we simply pass the reference to the constructor / create a new instance
        //
        ***************************/
        return new util;

    }
);