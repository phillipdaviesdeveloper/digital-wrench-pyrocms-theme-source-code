define(['modules/log', 'jquery'], function(log, $)
    {

        /************************************
        //
        // This module is used to calculate screen positions and fire animations
        // based on the passed critira.
        //
        ************************************/

        var that;

        function animposition()
        {

            // provides object scope reference in methods
            that = this;


            //ensure animationEngine the module is initialised correctly
            if (!(this instanceof animposition)) {
                throw new TypeError("Animposition constructor cannot be called as a function.");
            }

            // properties

            this.trigger = null;

            this.target = null;

            this.anim = null;

            this.modifer = null;

            this.type = 'removeClass';

            this.on = 'visible';

        }

            // "Private" methods

            /*************************
            //
            // These methods are used by the module specifically and have no external global use
            // or indeed any use by the client code.
            //
            *************************/

            /*************************
             //
             // Sets up the checking for animation trigger and the animation
             //
             **************************/
            function startWatcher()
            {



                // error out if config is incorrect
                if (that.target === null || that.anim === null ||that.trigger === null) { throw new Error('Animposition object not initialsied correctly') };

                //if the trigger is a DOM element
                if(isNaN(that.trigger))
                {

                    //finds the trigger item, if multiple items exist the first item found in the DOM is used
                    if($(that.trigger).length)
                        that.trigger = $(that.trigger);
                    else if($(that.trigger).length > 1)
                        that.trigger = $(that.trigger)[0];
                    else
                        throw new Error('DOM element was not found, please check your syntax');

                    //get the correct trigger position
                    getTriggerPosition();

                    //if there is a modifier apply it now
                    if(that.modifer !== null)
                        that.trigger = (that.trigger + that.modifer);

                }
                else
                {
                    //the trigger is a number so we only apply the modifer here
                    if(that.modifer !== null)
                        that.trigger = (that.trigger + that.modifer);
                }

            }

            /**
             * Sets the trigger position depending on the checking configuration set by the user
             */
            function getTriggerPosition()
            {
                if(that.on === 'scroll')
                    that.trigger = that.trigger.position().top+that.trigger.height();
                else
                    that.trigger = that.trigger.position().top;
            }



        // methods

        /*************************
        //
        // These methods are essentially the public getter, setter type to
        // be used by the client coder
        //
        *************************/

        animposition.prototype = {


                constructor: animposition,

                /****************************
                //
                //  sets up an animation position
                //
                //  target String -  Identifies the targeted DOM items
                //  anim String - Applies the animation class
                //  pos String or INT - either sets a defined pixel height or an element to pass
                //  modifer INT - allows a pixel amount to be added to tweak the animation trigger
                //  type String - either addClass or RemoveClass, default is removeClass
                //
                //
                ****************************/

                init: function(config)
                {
                    this.target = $(config.target);
                    this.anim = config.anim;
                    this.trigger = config.pos;


                    if(config.modifer !== null && !isNaN(config.modifer))
                        this.modifer = config.modifer;


                    if(config.type === 'addClass' || config.type === 'removeClass')
                        this.type = config.type;

                    if(config.on === 'scroll')
                        this.on = config.on;


                    startWatcher();

                },

                /**
                 *
                 * Takes the instance of the object to process the trigger / animations on a specific setInterval
                 *
                 * @param thats
                 * @returns {boolean}
                 */
                watchTrigger: function(thats)
                {

                    //check if the browser has passed the animation trigger
                    var check = ($(document).scrollTop()+$(window).height());

                    //or if configured, set check to when the trigger is visible
                    if(thats.on === 'scroll')
                    {
                        check = $(document).scrollTop();
                    }

                    //if the user has passed their configured trigger point
                    if(check >= thats.trigger)
                    {

                        //if a class needs to be added or removed
                        if(thats.type === 'addClass')
                        {
                            $(thats.target).each(function(){
                                $(this).addClass(thats.anim);
                            });
                        }
                        else
                        {
                            $(thats.target).each(function(){
                                $(this).removeClass(thats.anim);
                            });

                        }

                        return true;
                    }

                    return false;

                }


                // methods


        }



        // RequireJS return


        /**************************
        //
        // Usually the methods would be returned here, but as we are using a
        // object and a protype we simply pass the reference to the constructor / create a new instance
        //
        ***************************/
        return animposition;

    }
);