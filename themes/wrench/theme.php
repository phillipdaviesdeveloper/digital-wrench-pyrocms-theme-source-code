<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_wrench extends Theme
{
    public $name            = 'Wrench';
    public $author          = 'Phillip Davies';
    public $author_website  = 'http://digitalwrench.co.uk';
    public $website         = 'http://digitalwrench.co.uk';
    public $description     = 'Theme for http://digitalwrench.co.uk';
    public $version         = '1.0';

    public $options         =  array(
        'show_breadcrumbs' =>   array('title'         => 'Show Breadcrumbs',
            'description'   => 'Would you like to display breadcrumbs?',
            'default'       => 'yes',
            'type'          => 'radio',
            'options'       => 'yes=Yes|no=No',
            'is_required'   => TRUE),
        'layout' =>             array('title'         => 'Layout',
            'description'   => 'Which type of layout shall we use?',
            'default'       => '2 column',
            'type'          => 'select',
            'options'       => '2 column=Two Column|full-width=Full Width',
            'is_required'   => TRUE),
    );

}
/* End of file theme.php */