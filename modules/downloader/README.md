# PyroCMS Downloader Module

## Legal

This module was originally written by [Jerel Unruh](http://twitter.com/jerelunruh), one of the PyroCMS core developers but is now part of PyroCMS Documentation.
Here is his [blog post about this module](http://unruhdesigns.com/blog/2011/01/getting-started-with-custom-module-development-for-pyrocms).
If you find issues please open a ticket in the issue tracker here.

It is unlicensed, meaning that you can use it as a base for your own module without keeping the copyright intact. [visit unlicense.org](http://unlicense.org)
Please spread the news about PyroCMS and point new developers here if you like.
You may also be interested in [PyroCMS Workless](http://github.com/jerel/pyrocms-workless) a theme based on iKreativ's Workless.

## Description

This basic module was written to allow download urls to be created in the control panel.

Current limitations:

    1) not optimised for SEO, dead links will require pruning, active links may require adding to the sitemap.xml

    2) Does not check that links are valid for the system, therefore urls may collide if set in other parts of pyro (e.g. the page /download
    is created when you have setup a download link called downloads

    3) Currently it works off only the container that matches the base URI configured in the module settings (downloader/settings)

    4) Routes are created dynamically as a file in the app cache called 'routes.php' which is then imported before the 404_overide route in system/cms/cache.
       If this file is deleted and not generated again the applicaion may become unstable.


## Usage

To use this module simply clone it using Git or download the zip.  Once you have the folder you will need to rename it to "downloader". This is very important as
PyroCMS uses the folder name to determine the class to call when installing or when loading a controller. This may mean that you will need to unzip the
downloaded version and then rezip it before uploading to your PyroCMS install.