<?php
//messages
$lang['downloader:success']			=	'It worked';
$lang['downloader:error']			=	'It didn\'t work';
$lang['downloader:no_items']		=	'No Downloadable Links';

//page titles
$lang['downloader:create']			=	'Create Download';
$lang['downloader:settings']	    =	'Settings';

//labels
$lang['downloader:name']			=	'Name';
$lang['downloader:slug']			=	'Slug';
$lang['downloader:manage']			=	'Manage';
$lang['downloader:item_list']		=	'Link List';
$lang['downloader:view']			=	'View';
$lang['downloader:edit']			=	'Edit';
$lang['downloader:delete']			=	'Delete';
$lang['downloader:files']			=	'Linked file';
$lang['downloader:base']			=	'Base URI';
$lang['downloader:file']			=	'File';

//buttons
$lang['downloader:custom_button']	=	'Custom Button';
$lang['downloader:links']			=	'Downloadable Links';
?>