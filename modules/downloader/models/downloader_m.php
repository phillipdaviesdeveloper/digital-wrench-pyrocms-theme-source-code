<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This Class provides access to the download links database table
 *
 * @author 		Phillip Davies
 * @website		http://digitalwrench.co.uk
 * @package 	PyroCMS
 * @subpackage 	Downloader
 */
class Downloader_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();

        /**
         * Sets the Class table for queries
         */
		$this->_table = 'downloader';
	}


    /**
     * Creates a new download link record and updates the dynamic routes
     *
     * @param Array
     *
     * @return bool
     */
	public function create(array $input)
	{
        //hydrate the insert item
		$to_insert = array(
			'name' => $input['name'],
			'slug' => $this->_check_slug($input['slug']),
            'file' => $this->_clear_path($input['file'])
		);

        //insert the new item into the database
        if(!$this->db->insert('downloader', $to_insert))
        {
            return false;
        }

        //update the dynamic routes
		return $this->updateRoutes();
	}

    /**
     * Checks that the provided URI slug is in a valid format
     *
     * @param String
     *
     * @return String
     */
	public function _check_slug($slug)
	{
		$slug = strtolower($slug);
		$slug = preg_replace('/\s+/', '-', $slug);

		return $slug;
	}

    /**
     * Removes the {{url:site}} tag from the file path (why is this even there??)
     *
     * @param String
     *
     * @return String
     */
    public function _clear_path($path)
    {
        return preg_replace("/{{ url:site }}/", "", $path);
    }


    /**
     * Creates a routing file dynamically from all of the available download link items
     *
     * @return Bool
     */
    public function updateRoutes()
    {

        //get the base url
        $this->load->model('downloaderSettings_m');
        $base = $this->downloaderSettings_m->getBase();
        $base = $base[0]->base;

        //get of the routes to be generated
        $routes = $this->get_all();

        $output="<?php ";

        foreach($routes as $route)
        {
            $output.="\$route['".$base."/".$route->slug."']='".$route->file."';";
        }

        $this->load->helper('file');

        //write to the cache file
        if(write_file(APPPATH . "cache/routes.php", $output))
            return true;
        else
            return false;
    }
}
